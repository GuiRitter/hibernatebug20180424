package com.example.demo;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	UserService userService;

	// @Autowired
	@Resource(name="jackson")
	ObjectMapper mapper;

	@PersistenceContext
	EntityManager entityManager;

	@Test
	public void contextLoads() {
	}

	/**
	 * <b>UserService.java</b><br>
	 * <br>
	 * @author https://github.com/walfrat
	 * @version 5 Jan 2016
	 */
	@Test
	public void testSerialize() throws JsonProcessingException {
		User manager = new User(null, "manager", "ADDRESS THAT SHOULD NOT BE SHOWN");
		userService.create(manager);
		User anotherManager = new User(null, "eager manager", "ANOTHER ADDRESS THAT SHOULD NOT BE SHOWN");
		userService.create(anotherManager);
		User user = new User(null, "user", "address");
		user.setManager(manager);
		user.setAnotherManager(anotherManager);
		userService.create(user);

		user = userService.read(user.getId());
		user.getManager().getAddress();
		String value = mapper.writeValueAsString(user);
		System.err.println("testSerialize----------(lazyLoaded relation)");
		System.err.println(value);
	}
}
